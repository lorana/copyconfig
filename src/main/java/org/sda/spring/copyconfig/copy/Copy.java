package org.sda.spring.copyconfig.copy;

import org.sda.spring.copyconfig.reader.Reader;
import org.sda.spring.copyconfig.writer.Writer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("copy")
public class Copy {
	@Autowired
	@Qualifier("databaseWriter")
	private Writer writer;
	private Reader reader;

	@Autowired
	public Copy(@Qualifier("scannerReader")Reader reader) {
		super();
		this.reader = reader;
	}

	public Copy() {

	}

	public Writer getWriter() {
		return writer;
	}

	public void setWriter(Writer writer) {
		this.writer = writer;
	}

	public Reader getReader() {
		return reader;
	}

	public void setReader(Reader reader) {
		this.reader = reader;
	}

}
