package org.sda.spring.copyconfig.reader.impl;

import org.sda.spring.copyconfig.reader.Reader;
import org.springframework.stereotype.Component;

@Component("keyboardReader")
public class KeyboardReader implements Reader {

	@Override
	public String read() {

		return "message from keyboard";
	}
}
