package org.sda.spring.copyconfig.reader.impl;

import org.sda.spring.copyconfig.reader.Reader;
import org.springframework.stereotype.Component;

@Component("scannerReader")
public class ScannerReader implements Reader {
	
	@Override
	public String read() {

		return "message from scanner";
	}

}
