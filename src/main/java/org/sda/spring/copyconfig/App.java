package org.sda.spring.copyconfig;


import org.sda.spring.copyconfig.AppConfig;
import org.sda.spring.copyconfig.copy.Copy;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
	public static void main(String[] args) {
		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
		
		Copy copy = applicationContext.getBean("copy", Copy.class);
		String message = copy.getReader().read();
		copy.getWriter().write(message);

	}
}
