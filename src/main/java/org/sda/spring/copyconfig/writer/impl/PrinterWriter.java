package org.sda.spring.copyconfig.writer.impl;

import org.sda.spring.copyconfig.writer.Writer;
import org.springframework.stereotype.Component;
@Component("printerWriter")
public class PrinterWriter implements Writer{

	@Override
	public void write(String message) {
		
		System.out.println("Printing message to printer: " + message);

	}

}
