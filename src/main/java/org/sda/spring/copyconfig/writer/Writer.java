package org.sda.spring.copyconfig.writer;

public interface Writer {
	public void write(String message);

}
