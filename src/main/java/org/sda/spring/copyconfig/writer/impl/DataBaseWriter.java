package org.sda.spring.copyconfig.writer.impl;

import org.sda.spring.copyconfig.writer.Writer;
import org.springframework.stereotype.Component;
@Component("databaseWriter")
public class DataBaseWriter implements Writer {
	@Override
	public void write(String message) {
		System.out.println("Writing to DataBase: " + message);

	}
}
