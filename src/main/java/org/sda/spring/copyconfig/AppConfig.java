package org.sda.spring.copyconfig;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = "org.sda.spring.copyconfig")

@Configuration
public class AppConfig {

}
